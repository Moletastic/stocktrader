import Vue from 'vue';
import App from './App.vue';
import VueResource from 'vue-resource';
import router from './router';
import store from './store/store';

import 'materialize-css/dist/js/materialize.min.js';
import 'materialize-css/dist/css/materialize.min.css';

Vue.use(VueResource);
Vue.http.options.root = 'https://stock-trader-97b97.firebaseio.com/';

Vue.config.productionTip = false;

Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString() + ' USD';
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');


